/**
 * Created by MDFEROZ on 06/08/2021.
 */

var conn = require('./connection');
var db = conn.connection();
const geolocation = require('geolocation');

exports.index=function (req,res,next) {

    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="select * from vwusers where branchId="+branchId;
    db.query(sql,function (err,result) {
        res.render('jobs',{message:'Create New Job',users:result});
    })
};

exports.insertJob=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var post=req.body;

    var sql="insert into jobs(jobTitle,userId,assignBy,createdAt,branchId,lastDate)"+
            "values('"+post.jobTitle+"',"+post.cmbUsers+","+userId+",'"+dateFormat(new Date(), "yyyy/mm/dd hh:mm:ss")+"',"+branchId +
        ",'"+dateFormat(post.lastDate,"yyyy/mm/dd")+"')";
    db.query(sql,function (err,result) {
        message="Successfully Inserted";
        res.redirect('/createJob');
    })
};

exports.jobList=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

        sql="select * from vwJobs where userId="+ userId +" and status=0 order by name,dayRemain";
        db.query(sql,function (err,result) {
            if(err) res.send(err);
            sql="select * from vwJobs where  assignBy=" + userId+"  and userId<>"+userId+ " and status=0 order by name,dayRemain";
            db.query(sql,function (err,followupData) {
                if(err) res.send(err);
                res.render('jobList',{title:"Job List",data:result,fData:followupData});
            })

        })
}

exports.jobDone=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="update jobs set status=1 where id="+req.params.id;
    db.query(sql,function (err,result) {
        if(err) res.send(err);
        res.send(result);
    })
}

exports.jobComments=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="update jobs set comments='"+req.body.comments+"', updatedAt='"+dateFormat(new Date(), "yyyy/mm/dd hh:mm:ss")+"',"+
            " lat="+req.body.lat+", lon="+req.body.lon+ " where id="+req.body.id;
    db.query(sql,function (err,result) {
        if(err) res.send(err);
        res.send(req.body.comments);
    })
};

exports.userJob=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="select count(userId) as jobCount,userId,name,designation from vwjobs where status=0 and branchId="+branchId+
    " group by userId";
    db.query(sql,function (err,result) {
        if(err) res.send(err);
        res.render('userJob',{title:"User wise jobs",data:result});
    })
};

exports.userJobList=function (req,res,next) {
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    sql="select * from vwJobs where userId="+ req.params.userId +" and status=0 order by createdAt";
    db.query(sql,function (err,result) {
        if(err) res.send(err);
        res.render('userJobList',{title:"Users Job List",data:result});

    })

}
