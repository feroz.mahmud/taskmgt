

var conn = require('./connection');
var db = conn.connection();

//---------------------------------------------signup page call------------------------------------------------------
exports.signup = function(req, res){
    message = '';
    if(req.method == "POST"){
        var post  = req.body;
        var name= post.user_name;
        var pass= post.password;
        var fname= post.first_name;
        var lname= post.last_name;
        var mob= post.mob_no;

        var sql = "INSERT INTO `users`(`first_name`,`last_name`,`mob_no`,`user_name`, `password`) VALUES ('" + fname + "','" + lname + "','" + mob + "','" + name + "','" + pass + "')";

        var query = db.query(sql, function(err, result) {

            message = "Your account has been created.";
            res.render('signup',{message: message});
        });

    } else {
        res.render('signup');
    }
};

//-----------------------------------------------login page call------------------------------------------------------
exports.login = function(req, res){
    var message = '';
    var sess = req.session;

    if(req.method == "POST"){
        var post  = req.body;
        var name= post.user_name;
        var pass= post.password;

        var sql="SELECT * FROM `users` WHERE `user_name`='"+name+"' and password = '"+pass+"'";
        db.query(sql, function(err, results){
            if (err) res.send(err);
            else {
                if (results.length) {
                    req.session.userId = results[0].id;
                    req.session.user = results[0];
                    req.session.branchId=results[0].branchId;

                    res.redirect('/dashboard');
                }
                else {
                    message = 'Wrong User Name or Password.';
                    res.render('index',{message:message});
                }
            }

        });
    } else {
        res.render('index',{message:message});
    }

};
//-----------------------------------------------dashboard page functionality----------------------------------------------

exports.dashboard = function(req, res, next){
    var message = '';

    var user =  req.session.user;
    var userId = req.session.userId;
    var branchId= req.session.branchId;

    if(userId == null){
        res.redirect("/login");
        return;
    }
    var sql="SELECT * FROM `branches` where id="+branchId;
    db.query(sql,function (err,result) {
        if (err) res.send(err);
        userInfo=user.user_name+'@'+result[0].branchCode;
        sql="select * from branches ";
        db.query(sql,function (err,branchList) {
            if (err) res.send(err);
            res.render('dashboard', {message: message, title: 'DASHBOARD',user:user, branchId:branchId,branchList:branchList});
        })

    });

};
//--------------------------------------------------------------
exports.branchNavigation=function (req,res,next) {
    req.session.branchId=req.params.branchId;
    var user = req.session.user;
    userInfo=user.user_name+"@"+req.params.branchName;
    res.send(userInfo);
};



//------------------------------------logout functionality----------------------------------------------
exports.logout=function(req,res){
    req.session.destroy(function(err) {
        res.redirect("/login");
    })
};
//--------------------------------render user details after login--------------------------------
exports.profile = function(req, res){

    var userId = req.session.userId;
    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="SELECT * FROM `users` WHERE `id`='"+userId+"'";
    db.query(sql, function(err, result){
        res.render('profile',{data:result});
    });
};

//---------------------------------edit users details after login----------------------------------
exports.editprofile=function(req,res){
    var userId = req.session.userId;
    if(userId == null){
        res.redirect("/login");
        return;
    }

    var sql="SELECT * FROM `users` WHERE `id`='"+userId+"'";
    db.query(sql, function(err, results){
        res.render('edit_profile',{data:results});
    });
};
