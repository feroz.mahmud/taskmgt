var express = require('express');
var bodyParser = require('body-parser');
var config=require('./config');
var session = require('express-session');
var mysql = require('mysql');

var routes=require('./routes/index');
var user=require('./routes/user');

var jobs=require('./routes/jobs');


var app = express();
// var http = require('http').Server(app);
const https = require('https');
const fs = require('fs');
const options = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
};

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
global.path = require('path');
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: {maxAge: 3600 * 1000}
}));

var ip = require("ip");
global.host=ip.address();
console.log(ip.address()+":"+config.port);

global.dateFormat = require('dateformat');
var now = new Date();
console.log(dateFormat(now, "dd/mm/yyyy"));



app.get('/',routes.index);
app.get('/login', routes.index);//call for login page
app.post('/login', user.login);//call for login post
app.get('/dashboard', user.dashboard);//call for dashboard page after login
app.get('/home/logout', user.logout);//call for logout
// app.get('/branchNavigation/(:branchId)/(:branchName)',user.branchNavigation);

app.get('/createJob',jobs.index);
app.post('/createJob',jobs.insertJob);
app.get('/jobList',jobs.jobList);
app.get('/jobDone/(:id)',jobs.jobDone);
app.post('/jobComments',jobs.jobComments);
app.get('/userJob',jobs.userJob);
app.get('/userJobList/(:userId)',jobs.userJobList);



// http.listen(config.port, function(){
//   console.log('listening on '+host+':'+ config.port);
// });


https.createServer(options, app).listen(8000);
