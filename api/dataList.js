/**
 * Created by MDFEROZ on 03/11/2021.
 */

var mysql = require('mysql');
var conn = require('../routes/connection');
var db = conn.connection();

exports.getItemList=function (req,res,next) {
    var message = '';
    var branchId= req.session.branchId;
    var userId = req.session.userId;
    var user=req.session.user;

    if(userId == null){
        res.redirect("/login");
        return;
    }

    var QsrcBy='';
    var srcByValue=req.params.srcBy;
    var itemType=req.params.itemType;
    if(srcByValue!=''){ QsrcBy=" and (name like ('"+srcByValue+"%') or storeCode like ('"+srcByValue+"%') or partNo like ('"+srcByValue+"%'))"}

    var sql="select id,name,partNo,storeCode,concat(name,':',partNo,':',storeCode) as itemDescription,"+
        " currentStock,salesPrice,auditedQty,cardexQty,crippleStock,ckdStock,reevaluatedPrice,costPrice from itemaudit"+
        " where branchId="+branchId+" and itemtype ="+itemType+QsrcBy+
        " order by name";
    // console.log(sql);
    db.query(sql,function (err,result) {
        if(err) res.send(err);
        var option='<option value="0">---</option>';

        result.forEach(function(item) {
            option += "<option value=" + item.id + ">" + item.name + '[ CP: ' + item.costPrice + ' SP: ' + item.salesPrice + ' RP: '+item.reevaluatedPrice+' ]' + "</option>"
        });

        // Object.keys(result).forEach(function(key) {
        //     var row = result[key];
        //     option += "<option value=" + row.id + ">" + row.name + '[ Stock: ' + row.auditedQty + ' Price: ' + row.salesPrice + ' rePrice: '+row.reevaluatedPrice+' ]' + "</option>"
        // });

        // for (var i = 0; i < result.length; i++) {
        //     option += "<option value=" + result[i].id + ">" + result[i].name + '[ Qty: ' + result[i].currentStock + '-> ' + result[i].salesPrice + ']' + "</option>"
        // }
        res.send(option);
    });
};
